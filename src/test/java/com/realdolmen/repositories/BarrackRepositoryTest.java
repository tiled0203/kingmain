package com.realdolmen.repositories;

import com.realdolmen.domain.Knight;
import com.realdolmen.exceptions.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class BarrackRepositoryTest {
    private final BarrackRepository barrackRepository = new BarrackRepository();

    private final ResultSet resultSet = Mockito.mock(ResultSet.class);

    @BeforeEach
    public void setup() throws SQLException {
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt("id")).thenReturn(1);
        when(resultSet.getString("name")).thenReturn("name");
        when(resultSet.getString("gender")).thenReturn("MALE");
        when(resultSet.getDate("birth_date")).thenReturn(new Date(1));
        when(resultSet.getInt("health")).thenReturn(100);
        when(resultSet.getInt("health")).thenReturn(100);
        when(resultSet.getBigDecimal("address_id")).thenReturn(new BigDecimal(2));
        when(resultSet.getString("house_number")).thenReturn("house_number");
        when(resultSet.getString("street")).thenReturn("street");
        when(resultSet.getString("city")).thenReturn("city");
    }

    @Test
    void getAll() {
        List<Knight> all = barrackRepository.getAll();
        assertFalse(all.isEmpty());
    }

    @Test
    void findById() throws NotFoundException {
        Knight knight = barrackRepository.findById(1L);
        assertNotNull(knight);
        assertEquals(1, knight.getName());
    }

    @Test
    void trainAKnight() {
//        Assertions.assertDoesNotThrow(
//                () -> barrackRepository.trainAKnight(new People("test people", Gender.MALE, LocalDate.now()))
//        );

    }


 /*   @Test
    @DisplayName("This test is going to assert all fields in the knight")
    void testResultSet() throws SQLException {
        Knight knight = barrackRepository.mapKnight(resultSet);
        assertKnight(knight);
    }*/

   /* private void assertKnight(Knight knight) {
        Assertions.assertAll(
                () -> assertEquals(1, knight.getId()),
                () -> assertFalse(knight.getName().isEmpty()),
                () -> assertTrue(knight.getHealth() >= 0 && knight.getHealth() <= 100),
                () -> assertEquals(Gender.MALE, knight.getGender()),
                () -> assertEquals(2, knight.getAddress().getId()),
                () -> assertEquals("house_number", knight.getAddress().getHouseNumber()),
                () -> assertEquals("street", knight.getAddress().getStreet()),
                () -> assertEquals("city", knight.getAddress().getCity()),
                () -> assertEquals(LocalDate.of(1970, 1, 1), knight.getBirthDate()),
                () -> assertEquals(Profession.KNIGHT, knight.getProfession())
        );
    }*/
}