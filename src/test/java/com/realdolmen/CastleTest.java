package com.realdolmen;

import com.realdolmen.exceptions.NotFoundException;
import com.realdolmen.services.KnightService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;
import org.mockito.Mockito;

import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;

public class CastleTest {

    @Rule
    public SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Rule
    public final TextFromStandardInputStream systemInMock = emptyStandardInputStream();

    private final KnightService knightService = Mockito.mock(KnightService.class);

    private Castle castle;

    @Before
    public void setUp() throws Exception {
        castle= new Castle(knightService);
        doNothing().when(knightService).recruitKnight(anyString(),any(),any(), anyLong());
    }

    @Test
    public void showOptions() throws NotFoundException {
        systemInMock.provideLines("0");
        castle.throneRoom();
        String log = systemOutRule.getLog();
        Assert.assertEquals("what do you want to do?" + System.lineSeparator() +
                "0. Exit" + System.lineSeparator() +
                "1. Recruit a knight " + System.lineSeparator() +
                "2. Show a list of " + System.lineSeparator() +
                "3. Find a knight by id" + System.lineSeparator() +
                "4. Fire people" + System.lineSeparator() +
                "5. Train people" + System.lineSeparator(), log);
    }

    @Test
    public void useOption1() throws NotFoundException {
        systemInMock.provideLines("1", "Witcher", "MALE", "1990-03-02");
        castle.throneRoom();
        String log = systemOutRule.getLog();
        Assert.assertTrue( log.contains("What's the name, gender and date of birth of the knight?"));
    }
}