package com.realdolmen.services;

import com.realdolmen.domain.Knight;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class KnightServiceTest {
    private final KnightService knightService = new KnightService();

    @Test
    void getAll() {
        List<Knight> knights = knightService.getAll();
        Assertions.assertFalse(knights.isEmpty());
        Assertions.assertFalse(knights.get(0).getName().isEmpty());
    }
}