package com.realdolmen.util;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NumberUtilTest {

    @ParameterizedTest
    @CsvSource({"1,TRUE", "9,true", "10,FALSE", "3,TRUE", "8,FALSE", "7,TRUE", "2,FALSE"})
    void isOdd(String number, String expected) {
        assertEquals(Boolean.parseBoolean(expected), NumberUtil.isOdd(Integer.parseInt(number)));
    }

    @ParameterizedTest
    @MethodSource("provideNumbers")
    void isOddWithMethodSource(int number, boolean expected) {
        assertEquals(expected, NumberUtil.isOdd(number));
    }

    private static Stream<Arguments> provideNumbers() {
        return Stream.of(
                Arguments.of(1, true),
                Arguments.of(9, true),
                Arguments.of(10, false),
                Arguments.of(3, true),
                Arguments.of(8, false),
                Arguments.of(7, true)
        );
    }


}