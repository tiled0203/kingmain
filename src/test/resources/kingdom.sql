CREATE SCHEMA IF NOT EXISTS kingdom;
USE kingdom;


DROP TABLE IF EXISTS Knights;
CREATE TABLE Knights
(
    `id`         int NOT NULL AUTO_INCREMENT,
    `name`       varchar(45) DEFAULT NULL,
    `gender`     varchar(45) DEFAULT NULL,
    `birth_date` date        DEFAULT NULL,
    `health`     int,
    PRIMARY KEY (`id`)
);


DROP TABLE IF EXISTS TownRegister;
CREATE TABLE Address
(
    `id`           int NOT NULL AUTO_INCREMENT,
    `street`       varchar(45) DEFAULT NULL,
    `house_number` varchar(45) DEFAULT NULL,
    `city`         varchar(45) DEFAULT NULL,
    PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS TownRegister;
CREATE TABLE TownRegister
(
    `id`         int NOT NULL AUTO_INCREMENT,
    `address_id` int DEFAULT NULL,
    PRIMARY KEY (`id`)
);

INSERT INTO Address VALUES (1, 'street','1A','city');
INSERT INTO TownRegister VALUES (1, 1);

INSERT INTO Knights VALUES (1, 'Witcher', 'MALE', '1990-03-02', 100);
