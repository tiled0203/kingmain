package com.realdolmen.repositories;

import com.realdolmen.domain.Farmer;
import com.realdolmen.domain.People;
import com.realdolmen.util.PropertiesReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FarmerRepository {
    private static final String PASSWORD = PropertiesReader.getInstance().getProperty("db.password");
    private static final String USER = PropertiesReader.getInstance().getProperty("db.user");
    private static final String DB_URL = PropertiesReader.getInstance().getProperty("db.url");
    private final Logger logger = LoggerFactory.getLogger(FarmerRepository.class);


    public void trainAFarmer(People people) {

    }

    public List<Farmer> getAll() {
        try (Connection connection = DriverManager.getConnection(DB_URL, USER, PASSWORD); Statement statement = connection.createStatement()) {
            List<Farmer> farmers = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery("select * from Farmers as f left join TownRegister as tr on tr.id = f.id left join Address as a ON tr.address_id = a.id ;");
            while (resultSet.next()) {
                farmers.add(mapFarmer(resultSet));
            }
            return farmers;
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return Collections.emptyList();
        }
    }

    private Farmer mapFarmer(ResultSet resultSet) throws SQLException {
        return null;
    }
}
