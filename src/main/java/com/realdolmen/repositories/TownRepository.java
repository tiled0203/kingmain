package com.realdolmen.repositories;

import com.realdolmen.domain.People;
import com.realdolmen.domain.Profession;
import com.realdolmen.util.PropertiesReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class TownRepository {
    private static final String PASSWORD = PropertiesReader.getInstance().getProperty("db.password");
    private static final String USER = PropertiesReader.getInstance().getProperty("db.user");
    private static final String DB_URL = PropertiesReader.getInstance().getProperty("db.url");
    private final Logger logger = LoggerFactory.getLogger(TownRepository.class);


    public List<People> getAll() {
        return null;

    }

    private People mapPeople(ResultSet resultSet) throws SQLException {
        return null;
//        return new People(resultSet.getInt("tr.id"), resultSet.getString("name"), Gender.valueOf(resultSet.getString("gender")), resultSet.getDate("birth_date").toLocalDate());
    }

    public List<People> findAllByOccupation(Profession profession) {
        return null;

    }

    public void update(People people) {

    }

    public People registerNewPeopleToTownRegister(People people) {
        return null;

    }

    public People findRandomPeopleByProfession(Profession profession) {
        return null;

    }

    public void firePeopleById(int id) {

    }

    public People findById(int id) {

        return null;
    }
}
