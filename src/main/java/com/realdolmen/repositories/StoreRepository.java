package com.realdolmen.repositories;

import com.realdolmen.util.PropertiesReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StoreRepository {
    private static final String PASSWORD = PropertiesReader.getInstance().getProperty("db.password");
    private static final String USER = PropertiesReader.getInstance().getProperty("db.user");
    private static final String DB_URL = PropertiesReader.getInstance().getProperty("db.url");
    private Logger logger = LoggerFactory.getLogger(StoreRepository.class);


}
