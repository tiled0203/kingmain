package com.realdolmen.repositories;

import com.realdolmen.domain.Nurse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class HospitalRepository {
    private final Logger logger = LoggerFactory.getLogger(HospitalRepository.class);

    private final EntityManager entityManager;

    public HospitalRepository() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("kingPu");
        this.entityManager = factory.createEntityManager();
    }

    public void trainANurse(Nurse nurse) {
        entityManager.getTransaction().begin();

        entityManager.merge(nurse);
        entityManager.getTransaction().commit();
    }


    public List<Nurse> getAll() {
        return entityManager.createQuery("select n from Nurse n").getResultList();
    }
}
