package com.realdolmen.repositories;

import com.realdolmen.domain.BlackSmith;
import com.realdolmen.domain.People;
import com.realdolmen.util.PropertiesReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

//some comment
public class BlackSmithRepository {
    private static final String PASSWORD = PropertiesReader.getInstance().getProperty("db.password");
    private static final String USER = PropertiesReader.getInstance().getProperty("db.user");
    private static final String DB_URL = PropertiesReader.getInstance().getProperty("db.url");
    private final Logger logger = LoggerFactory.getLogger(BlackSmithRepository.class);


    public void trainABlackSmith(People people) {

    }

    public List<BlackSmith> getAll() {
      return null;
    }

    private BlackSmith mapBlackSmith(ResultSet resultSet) throws SQLException {
        return null;
    }
}
