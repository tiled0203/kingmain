package com.realdolmen.repositories;

import com.realdolmen.domain.Address;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class AddressRepository {
    private final EntityManager entityManager;

    public AddressRepository() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("kingPu");
        this.entityManager = factory.createEntityManager();
    }
    
    public List<Address> findAll(){
        return entityManager.createQuery("select a from Address a").getResultList();
    }

    public Address findById(Long id) {
        return entityManager.find(Address.class, id);
    }
}
