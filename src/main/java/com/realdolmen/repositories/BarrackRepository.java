package com.realdolmen.repositories;

import com.realdolmen.domain.Knight;
import com.realdolmen.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.List;

public class BarrackRepository {
	private final Logger logger = LoggerFactory.getLogger(BarrackRepository.class);
	private final EntityManager entityManager;

	public BarrackRepository() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("kingPu");
		this.entityManager = factory.createEntityManager();
	}

	public void trainAKnight(Knight knight) {
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		try {
			this.entityManager.merge(knight);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			e.printStackTrace();
		}
	}

	public List<Knight> getAll() {
		List<Knight> knights = entityManager.createQuery("select k from Knight k", Knight.class)
				.getResultList();
		entityManager.clear();
		return knights;
	}

	public Knight findById(Long id) throws NotFoundException {
		Knight knight = entityManager.find(Knight.class, id);
		return knight;
	}

	public void update(Knight knight) {
		entityManager.getTransaction().begin();
		try {
			Knight knight1 = entityManager.find(Knight.class, knight.getId(), LockModeType.OPTIMISTIC_FORCE_INCREMENT);
			knight1.setName("dslkufezif");
//			sleep(20000);
			entityManager.merge(knight1);
			entityManager.getTransaction().commit();
		} catch (Exception exception) {
			entityManager.getTransaction().rollback();
			exception.printStackTrace();
		}
	}
}
