package com.realdolmen;

import com.realdolmen.domain.*;
import com.realdolmen.exceptions.NotFoundException;
import com.realdolmen.repositories.AddressRepository;
import com.realdolmen.services.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;
import java.util.StringJoiner;
import java.util.logging.Level;

public class Castle {
    private final Logger logger = LoggerFactory.getLogger(Castle.class);
    private final KnightService knightService;
    private final BlackSmithService blackSmithService;
    private final HealingService healingService = new HealingService();
    private final FarmerService farmerService = new FarmerService();
    private final TownService townService = new TownService();
    private final Scanner scanner = new Scanner(System.in);
    private final AddressRepository addressRepository = new AddressRepository();

    private static boolean isRunning = true;

    Castle(KnightService knightService) {
        this.knightService = knightService;
        this.blackSmithService = new BlackSmithService();
    }

    public Castle() {
        java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);

        blackSmithService = new BlackSmithService();
        knightService = new KnightService();
    }

    public static void main(String[] args) {
        new Castle().updateKnight();
//        try {
//            Class.forName(PropertiesReader.getInstance().getProperty("db.driverClassName"));
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        Castle castle = new Castle();
//        while (isRunning) {
//            try {
//                castle.throneRoom();
////                castle.experimental();
//
//            } catch (NotFoundException e) {
//                e.printStackTrace();
//            }
//        }
    }

    private void updateKnight() {
        Knight knight = new Knight(1L);
        knightService.update(knight);

    }

    private void experimental() {
        addressRepository.findAll().stream().forEach(address -> System.out.println(address));
    }

    void throneRoom() throws NotFoundException {
        System.out.println("what do you want to do?");
        System.out.println("0. Exit");
        System.out.println("1. Recruit a knight ");
        System.out.println("2. Show a list of ");
        System.out.println("3. Find a knight by id");
        System.out.println("4. Fire people");
        System.out.println("5. Train people");
        System.out.println("6. Train nurse");

        int option = scanner.nextInt();
        scanner.nextLine();
        switch (option) {
            case 0:
                Castle.isRunning = false;
                break;
            case 1:
                createKnight();
                break;
            case 2:
                showListOf();
                break;
            case 3:
                findKnightById();
                break;
            case 4:
                firePeople();
                break;
            case 5:
                trainPeople();
                break;
            case 6:
                trainNurse();
                break;
            default:
                System.out.println("Invalid selection");
                break;
        }


    }

    private void trainNurse() {
        System.out.println("What's the name, gender and date of birth of the nurse?");
        String name = scanner.nextLine();
        Gender gender = Gender.valueOf(scanner.nextLine());
        LocalDate birthDate = LocalDate.parse(scanner.nextLine());
        System.out.println("What's the id for the address?");
        healingService.trainNurse(new People(name,gender,birthDate, new Address(scanner.nextLong())));
    }

    private void trainPeople() {
        People people = townService.findRandomByProfession(Profession.NONE);
        System.out.printf("%s with id %s is selected ", people.getName(), people.getId());
        System.out.println("What kind of training does he/she needs?");

        System.out.println("\t1. Knight");
        System.out.println("\t2. Farmer");
        System.out.println("\t3. Blacksmith");
        System.out.println("\t4. Nursing");

        switch (scanner.nextInt()) {
            case 1:
                knightService.trainKnight(people);
                break;
            case 2:
                farmerService.trainFarmer(people);
                break;
            case 3:
                blackSmithService.trainBlackSmith(people);
                break;
            case 4:
                healingService.trainNurse(people);
                break;
            default:
                System.out.println("Invalid selection");
                break;
        }
    }

    private void firePeople() {
        System.out.println("What is the profession of the person?");
        showListOf();
        System.out.println("Who are we going to fire? Give his/her id:");
        townService.firePeopleById(scanner.nextInt());
    }

    private void findKnightById() throws NotFoundException {
        System.out.println("What is the uid of the knight?");
        Long id = scanner.nextLong();
        Knight knight = knightService.findKnightById(id);
        System.out.println("name:" + knight.getName() + " addr:" + knight.getAddress());
        /* String format = "%4s|%20s|%7s|%11s|%3s|%30s%n";
        System.out.printf(format, "id", "name", "gender", "date of birth", "health", "address");
        String address = "NO ADDRESS";
        if (knight.getAddress() != null) {
            address = knight.getAddress().getStreet() + " " + knight.getAddress().getHouseNumber() + ", " + knight.getAddress().getCity();
        }
        System.out.printf(format, knight.getId(), knight.getName(), knight.getGender(), knight.getBirthDate(), knight.getHealth(), address);*/
    }

    private void showListOf() {
        System.out.println("\t1. Knights");
        System.out.println("\t2. Farmers");
        System.out.println("\t3. Blacksmiths");
        System.out.println("\t4. Nurses");

        switch (scanner.nextInt()) {
            case 1:
                List<Knight> allKnights = knightService.getAll();
                System.out.println(allKnights.isEmpty() ? "----No knights found----" : "");
                for (Knight knight : allKnights) {
                    System.out.println("name:" + knight.getName() );
                }
                // printTable(allKnights, "id", "name", "birthDate");
                break;
            case 2:
                List<Farmer> farmers = farmerService.getAll();
                System.out.println(farmers.isEmpty() ? "----No farmers found----" : "");
                printTable(farmers, "id", "name", "birthDate");
                break;
            case 3:
                List<BlackSmith> blackSmiths = blackSmithService.getAll();
                System.out.println(blackSmiths.isEmpty() ? "----No black smiths found----" : "");
                printTable(blackSmiths, "id", "name", "birthDate");
                break;
            case 4:
                List<Nurse> nurses = healingService.getAll();
                System.out.println(nurses.isEmpty() ? "----No nurses found----" : "");
                printTable(nurses, "id", "name", "birthDate");
                break;
            default:
                System.out.println("Invalid selection");
                break;
        }

    }

    private void printTable(List<? extends Human> list, String... tableHeaders) {
        if (!list.isEmpty()) {
            try {
                StringJoiner headers = new StringJoiner("|");
                for (int i = 0; i < tableHeaders.length; i++) {
                    headers.add("%30s");
                }
                String format = headers + " | %n";
                System.out.printf(format , tableHeaders);
                for (Human human : list) {
                    for (String tableHeader : tableHeaders) {
                        Field declaredField = list.get(0).getClass().getSuperclass().getField(tableHeader);
                        System.out.printf("%30s | ", declaredField.get(human).toString());
                    }
                    System.out.println();
                }
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private void createKnight() throws NotFoundException {
        System.out.println("What's the name, gender and date of birth of the knight?");
        String name = scanner.nextLine();
        Gender gender = Gender.valueOf(scanner.nextLine());
        LocalDate birthDate = LocalDate.parse(scanner.nextLine());
        System.out.println("What's the id for the address?");
        knightService.recruitKnight(name, gender, birthDate, scanner.nextLong());

    }

}
