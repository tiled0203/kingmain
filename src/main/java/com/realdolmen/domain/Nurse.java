package com.realdolmen.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
@DiscriminatorValue("NURSE")
public class Nurse extends Human{

    private int healingPower;



    public Nurse() {
        super();
    }

    public Nurse(String name, Gender gender, LocalDate birthDate, Address address, int healingPower) {
        super(name, gender, birthDate, address);
        this.healingPower = healingPower;
    }

    public int getHealingPower() {
        return healingPower;
    }

    public void setHealingPower(int healingPower) {
        this.healingPower = healingPower;
    }
}
