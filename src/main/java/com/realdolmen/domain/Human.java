package com.realdolmen.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "profession", discriminatorType = DiscriminatorType.STRING)
public class Human {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	@ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.REFRESH})
	private Address address;

	@Column(name = "profession", insertable = false, updatable = false)
	@Enumerated(EnumType.STRING)
	private Profession profession;

	@Enumerated(EnumType.STRING)
	private Gender gender;
	private LocalDate birthDate;
	@Version
	private Integer version;
	public Human() {
	}

	protected Human(String name, Gender gender, LocalDate birthDate, Address address) {
		this.name = name;
		this.gender = gender;
		this.birthDate = birthDate;
		this.address = address;
	}

	protected Human(Long id, String name, Gender gender, LocalDate birthDate) {
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.birthDate = birthDate;
	}

	public Human(long id) {
		this.id = id;
	}


	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public Gender getGender() {
		return gender;
	}

	public Profession getProfession() {
		return profession;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
}
