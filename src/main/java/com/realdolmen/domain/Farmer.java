package com.realdolmen.domain;

import java.time.LocalDate;

public class Farmer extends Human{
    public Farmer(Long id, String name, Gender gender, LocalDate birthDate) {
        super(id, name, gender, birthDate);
    }

}
