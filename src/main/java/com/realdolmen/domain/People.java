package com.realdolmen.domain;

import java.time.LocalDate;

public class People extends Human {
    public People(Long id, String name, Gender gender, LocalDate birthDate) {
        super(id, name, gender, birthDate);
    }

    public People(String name, Gender gender, LocalDate birthDate, Address address ) {
        super(name, gender, birthDate,  address);
    }
}
