package com.realdolmen.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.time.LocalDate;
import java.time.Period;


@Entity
//@IdClass(value = KnightId.class)
@DiscriminatorValue("KNIGHT")
public class Knight extends Human{

    private int health;

    @Transient
	private Integer age;

	public Knight() {
		super();
	}

	public Knight(String name, LocalDate birthDate, Gender gender, Address address) {
		super(name,gender,birthDate, address);
	}

	public Knight(long id) {
		super(id);
	}

	public Integer getAge() {
		 age = Period.between(getBirthDate(),LocalDate.now()).getYears();
		 return age;
	}

	public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }



}
