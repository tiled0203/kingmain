package com.realdolmen.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class KnightDto {
    private final String name;
    private List<Address> addresses = new ArrayList<>();

    public KnightDto(String name, List<Address> addresses) {
        this.name = name;
        this.addresses = addresses;
    }

    public String getName() {
        return name;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", KnightDto.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("addresses=" + addresses)
                .toString();
    }
}
