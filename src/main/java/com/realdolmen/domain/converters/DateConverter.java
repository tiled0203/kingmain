package com.realdolmen.domain.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Converter(autoApply = true)
public class DateConverter implements AttributeConverter<LocalDate, Date> {
    @Override
    public Date convertToDatabaseColumn(LocalDate attribute) {
        return Date.from(attribute.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

    @Override
    public LocalDate convertToEntityAttribute(Date dbDate) {
        return dbDate.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }
}
