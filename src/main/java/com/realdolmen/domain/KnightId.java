package com.realdolmen.domain;

import javax.persistence.Column;
import java.io.Serializable;
import java.time.LocalDate;

public class KnightId implements Serializable {
    private String name;
    @Column(length = 255)
    private LocalDate birthDate;

    public KnightId(String name, LocalDate birthDate) {
        this.name = name;
        this.birthDate = birthDate;
    }

    public KnightId() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
}
