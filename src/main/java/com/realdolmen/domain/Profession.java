package com.realdolmen.domain;

public enum Profession {
    NONE,KNIGHT,NURSE,BLACKSMITH,FARMER;
}
