package com.realdolmen;

import com.github.javafaker.Faker;
import com.realdolmen.domain.Address;
import com.realdolmen.domain.Gender;
import com.realdolmen.domain.Knight;
import com.realdolmen.repositories.BarrackRepository;

import java.time.LocalDate;
import java.time.ZoneId;

public class FillDataBase {
    private static final BarrackRepository barrackRepository = new BarrackRepository();

    public static void main(String[] args) {
        for (int i = 0; i < 50; i++) {
            LocalDate date = Faker.instance().date().birthday(20, 80).toInstant().atZone(ZoneId.systemDefault())
                    .toLocalDate();
            com.github.javafaker.Address address = Faker.instance().address();
            Address address1 = new Address(address.streetAddress(), address.streetAddressNumber(), address.city());
            Knight knight = new Knight(Faker.instance().funnyName().name(), date, Gender.values()[i % 2], address1);


            barrackRepository.trainAKnight(knight);
        }
    }
}
