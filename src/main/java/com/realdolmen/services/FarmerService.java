package com.realdolmen.services;

import com.realdolmen.domain.Farmer;
import com.realdolmen.domain.People;
import com.realdolmen.repositories.FarmerRepository;
import com.realdolmen.repositories.StoreRepository;
import com.realdolmen.repositories.TownRepository;

import java.util.List;

public class FarmerService {
    private final StoreRepository storeRepository = new StoreRepository();
    private final TownRepository townRepository = new TownRepository();
    private final FarmerRepository farmerRepository = new FarmerRepository();

    public void trainFarmer(People people) {
        farmerRepository.trainAFarmer(people);
        townRepository.update(people);
    }

    public List<Farmer> getAll() {
        return farmerRepository.getAll();
    }


}
