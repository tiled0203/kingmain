package com.realdolmen.services;

import com.realdolmen.domain.BlackSmith;
import com.realdolmen.domain.People;
import com.realdolmen.repositories.BlackSmithRepository;
import com.realdolmen.repositories.StoreRepository;
import com.realdolmen.repositories.TownRepository;

import java.util.List;

public class BlackSmithService {
    private final StoreRepository storeRepository = new StoreRepository();
    private final TownRepository townRepository = new TownRepository();
    private final BlackSmithRepository blackSmithRepository = new BlackSmithRepository();

    public void trainBlackSmith(People people) {
        blackSmithRepository.trainABlackSmith(people);
        townRepository.update(people);
    }

    public List<BlackSmith> getAll() {
        return blackSmithRepository.getAll();
    }

}
