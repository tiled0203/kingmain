package com.realdolmen.services;

import com.realdolmen.domain.Address;
import com.realdolmen.domain.Gender;
import com.realdolmen.domain.Knight;
import com.realdolmen.domain.People;
import com.realdolmen.exceptions.NotFoundException;
import com.realdolmen.repositories.BarrackRepository;
import com.realdolmen.repositories.TownRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.List;

public class KnightService {
    private final Logger logger = LoggerFactory.getLogger(KnightService.class);
    private final AddressService addressService = new AddressService();
    private final BarrackRepository barrackRepository;
    private final TownRepository townRepository = new TownRepository();

    public KnightService() {
        this.barrackRepository = new BarrackRepository();
    }

    public void recruitKnight(String name, Gender gender, LocalDate birthDate, Long addressId) throws NotFoundException {
        Address address = new Address();
        address.setStreet("zieufehgofiezgfoi");
        address.setId(addressId);
        Knight knight = new Knight(name,birthDate,gender,address);

        barrackRepository.trainAKnight(knight);
        logger.info("a new knight named {} with birthdate {} is added to the towns register and to the knights table", name, knight.getBirthDate());
    }

    public List<Knight> getAll() {
        return barrackRepository.getAll();
    }

    public Knight findKnightById(Long id) throws NotFoundException {
        return barrackRepository.findById(id);
    }

    public void trainKnight(People people) {
        Knight knight = new Knight(people.getName(),people.getBirthDate(),people.getGender(),new Address());
        barrackRepository.trainAKnight(knight);
        townRepository.update(people);
    }

    public void update(Knight knight){
        barrackRepository.update(knight);
    }
}
