package com.realdolmen.services;

import com.realdolmen.domain.Address;
import com.realdolmen.exceptions.NotFoundException;
import com.realdolmen.repositories.AddressRepository;

public class AddressService {
    private final AddressRepository addressRepository = new AddressRepository();

    public Address findAddressById(Long id) throws NotFoundException {
        return addressRepository.findById(id);
    }


}
