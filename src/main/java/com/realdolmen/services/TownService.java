package com.realdolmen.services;

import com.realdolmen.domain.People;
import com.realdolmen.domain.Profession;
import com.realdolmen.repositories.TownRepository;

import java.util.List;

public class TownService {
    private final TownRepository townRepository = new TownRepository();

    public List<People> getAll() {
        return townRepository.getAll();
    }

    public List<People> findAllByOccupation(Profession profession) {
        return townRepository.findAllByOccupation(profession);
    }

    public People findRandomByProfession(Profession profession) {
        return townRepository.findRandomPeopleByProfession(profession);
    }

    public void firePeopleById(int id) {
        townRepository.firePeopleById(id);
        People people = townRepository.findById(id);
        townRepository.update(people);
    }


}
