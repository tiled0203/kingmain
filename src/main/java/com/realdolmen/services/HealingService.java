package com.realdolmen.services;

import com.realdolmen.domain.Nurse;
import com.realdolmen.domain.People;
import com.realdolmen.repositories.HospitalRepository;
import com.realdolmen.repositories.TownRepository;

import java.util.List;

public class HealingService {
    private final HospitalRepository hospitalRepository = new HospitalRepository();
    private final TownRepository townRepository = new TownRepository();

    public void trainNurse(People people) {
        Nurse nurse = new Nurse(people.getName(), people.getGender(), people.getBirthDate(), people.getAddress(), 100);
        hospitalRepository.trainANurse(nurse);
//        townRepository.update(people);
    }

    public List<Nurse> getAll() {
        return hospitalRepository.getAll();
    }

}
