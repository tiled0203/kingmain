package com.realdolmen.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {


	private static final PropertiesReader propertiesReader = new PropertiesReader();

	private PropertiesReader() {

	}

	public static PropertiesReader getInstance() {
		return propertiesReader;
	}

	public String getProperty(String propertyName) {
		Properties properties = null;
		try {
			InputStream is = getClass().getClassLoader().getResourceAsStream("config.properties");
			properties = new Properties();
			properties.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties.getProperty(propertyName);
	}

}
