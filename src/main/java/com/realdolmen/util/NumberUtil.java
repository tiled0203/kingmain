package com.realdolmen.util;

public class NumberUtil {

    private NumberUtil() {
    }

    public static boolean isOdd(int number) {
        return number % 2 != 0;
    }
}
